Feature: Confirm whether a visa is required to visit the UK
  based on the nationality, visa reason and conditions.

  Background: User is on the Visa home page
    Given the user navigates to the visa page

  @uitests @uitest_1
  Scenario Outline: Visa result should be informed based on nationality and visit purpose
    And provides a nationality of <Country>
    And selects the reason <Visa Reason>
    And states <Additional Conditions>
    When the user submits the form
    Then the user will be informed of the <Visa Result>

    Examples:
      | Country | Visa Reason | Additional Conditions          | Visa Result                                             |
      | Japan   | Study       | Duration more than 6 Months    | You’ll need a visa to study in the UK                   |
      | Japan   | Tourism     | None                           | You do not need a visa if you’re staying up to 6 months |
      | Russia  | Tourism     | Not visiting partner or family | You’ll need a visa to come to the UK                    |