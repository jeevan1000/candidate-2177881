webdriver {
  driver = chrome
  use.driver.service.pool = false
  timeouts {
    implicitlywait = 5000
    fluentwait = 10000
  }
}

headless.mode = false

#
# Chrome options can be defined using the chrome.switches property
#
chrome.switches = """--start-maximized;--test-type;--no-sandbox;--ignore-certificate-errors;
                   --disable-popup-blocking;--disable-default-apps;--disable-extensions-file-access-check;
                   --incognito;--disable-infobars,--disable-gpu"""
#
# Define drivers for different platforms. Serenity will automatically pick the correct driver for the current platform
#
drivers {
  windows {
    webdriver.chrome.driver = "src/test/resources/webdriver/windows/chromedriver.exe"
    webdriver.gecko.driver = "src/test/resources/webdriver/windows/geckodriver.exe"
    webdriver.ie.driver = "src/test/resources/webdriver/windows/IEDriverServer.exe"
  }
  mac {
    webdriver.chrome.driver = "src/test/resources/webdriver/mac/chromedriver"
    webdriver.gecko.driver = "src/test/resources/webdriver/mac/geckodriver"
  }
}

#
# This section defines environment-specific configuration for different environments.
# You can define normal Serenity properties, such as webdriver.base.url, or custom ones
#

environments {
  default {
    webdriver.base.url = "https://www.gov.uk/"
  }
  dev {
    webdriver.base.url = "https://DEV.gov.uk/"
  }
  staging {
    webdriver.base.url = "https://STG.gov.uk/"
  }
  prod {
    webdriver.base.url = "https://www.gov.uk/"
  }
  all {
    home.page = "#{webdriver.base.url}"
    visa.page = "#{webdriver.base.url}/check-uk-visa/y"
    postcodes.api.endpoint = "https://api.postcodes.io/postcodes/"
  }
}
serenity.take.screenshots=FOR_FAILURES

serenity.project.name="HomeOffice Assignment"
serenity.requirement.types= "feature"
serenity.browser.maximized = true


# configs to run tests remotely (ex: GRID) uncomment the following webdriver section.webdriver

#
#   webdriver {
#       driver = remote
#       remote {
#           url="http://localhost:4444/wd/hub"
#           driver=chrome
#       }
#   }
#