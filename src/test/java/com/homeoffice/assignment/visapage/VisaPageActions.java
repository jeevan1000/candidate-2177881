package com.homeoffice.assignment.visapage;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class VisaPageActions extends UIInteractionSteps {

    /*
     * This class contain the actions/steps that can be performed on Visa page
     * BASE_URL will be fetched from the serenity.conf file with EnvironmentSpecificConfiguration and getProperty methods
     */

    VisaHomePage visaHomePage;

    @Step("User is on the Visa home page")
    public void goToVisaHomePage() {
        visaHomePage.open();
    }

    @Step("User selects the country of nationality as {0}")
    public void selectCountry(String country) {
        $(VisaHomePage.COUNTRY_SELECT).select().byVisibleText(country);
        nextStep();
    }

    @Step(("User continues to next step"))
    public void nextStep() {
        $(VisaHomePage.NEXT_STEP_BTN).click();
    }

    @Step("User selects the reason as {0}")
    public void selectReason(String reason) {
        if (reason.equalsIgnoreCase("STUDY")) {
            $(VisaHomePage.STUDY_RADIO).click();
            nextStep();
        } else if (reason.equalsIgnoreCase("TOURISM")) {
            $(VisaHomePage.TOURISM_RADIO).click();
            nextStep();
        }
    }

    @Step(("User selects the condition as {0}"))
    public void selectCondition(String condition) {
        if (condition.equalsIgnoreCase("Duration more than 6 months")) {
            $(VisaHomePage.MORE_THAN_SIX_MONTHS).click();
            nextStep();
        } else if (condition.equalsIgnoreCase("Not visiting partner or family")) {
            $(VisaHomePage.VISIT_PARTNER_NO).click();
            nextStep();
        }
    }

}
