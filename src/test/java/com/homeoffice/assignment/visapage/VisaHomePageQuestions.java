package com.homeoffice.assignment.visapage;

import net.thucydides.core.annotations.Step;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class VisaHomePageQuestions extends VisaHomePage {

    /*
     * This class contains all the assertions that can be made on the visa page
     * SoftAssertions are used which will execute all the steps even if there is failure, which helps_
     * to fix other assertions without a need to re-run the tests
     */

    @Step("Verify the Visa result confirmation message")
    public void verifyVisaResultConfirmationMsg(String expectedVisaResultMsg) {
        String actualVisaResult = waitForCondition().until(
                ExpectedConditions.visibilityOfElementLocated(VisaHomePage.VISA_RESULT)).getText();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(expectedVisaResultMsg.equalsIgnoreCase(actualVisaResult)).isTrue();
        softly.assertAll();
    }
}