package com.homeoffice.assignment.visapage;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.openqa.selenium.By;

@DefaultUrl("page:visa.page")
class VisaHomePage extends PageObject {

    /*
     * This class acts as a object repository for Visa page and lists all the Webelements that we need to use.
     * @DefaultUrl is fetched from the serenity.conf file which will be base URL for this page
     */

    static private final EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
    static final String VISAHOMEPAGE_URL = EnvironmentSpecificConfiguration.from(env).getProperty("visa.page");

    static final By COUNTRY_SELECT = By.cssSelector(".govuk-select");
    static final By TOURISM_RADIO = By.cssSelector("input[value='tourism']");
    static final By STUDY_RADIO = By.cssSelector("input[value='study']");
    static final By NEXT_STEP_BTN = By.cssSelector("#current-question > button");
    static final By VISA_RESULT = By.cssSelector("h2[class*='gem-c-heading']");
    static final By MORE_THAN_SIX_MONTHS = By.cssSelector("input[value='longer_than_six_months']");
    static final By VISIT_PARTNER_NO = By.cssSelector("input[value='no']");
}

