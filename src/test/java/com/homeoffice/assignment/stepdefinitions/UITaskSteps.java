package com.homeoffice.assignment.stepdefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.homeoffice.assignment.visapage.VisaHomePageQuestions;
import com.homeoffice.assignment.visapage.VisaPageActions;

public class UITaskSteps {

    /*
    * @Steps VisaPageActions contain the business level steps that can be performed on visa page
    * @Steps VisaHomePageQuestions contain assertions that can be done on the visa page
    */

    @Steps
    VisaPageActions visaPageActions;

    @Steps
    VisaHomePageQuestions visaHomePageQuestions;

    @Given("the user navigates to the visa page")
    public void theUserNavigatesToVisaPage() {
        visaPageActions.goToVisaHomePage();
    }

    @And("^provides a nationality of (.+)$")
    public void providesNationalityOf(String country) {
        visaPageActions.selectCountry(country);
    }

    @When("^the user submits the form$")
    public void submitsTheForm() {
    }

    @And("^selects the reason (.+)$")
    public void selectsTheReason(String visareason) {
        visaPageActions.selectReason(visareason);
    }

    @And("^states (.*)$")
    public void states(String condition) {
        visaPageActions.selectCondition(condition);
    }

    @Then("^the user will be informed of the (.+)$")
    public void userWillBeInformed(String visaresult) {
        visaHomePageQuestions.verifyVisaResultConfirmationMsg(visaresult);
    }
}
