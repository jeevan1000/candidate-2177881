package com.homeoffice.assignment.stepdefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Steps;
import java.util.List;

import com.homeoffice.assignment.apiutils.CommonQuestions;
import com.homeoffice.assignment.apiutils.PostCodeAPIActions;

public class PostCodeAPISteps {

    /*
     * @Steps PostCodeAPIActions contain the steps that can be called on PostCode API endpoint
     * @Steps CommonQuestions contain generic assertions that can be queried on any endpoint
     */

    @Steps
    PostCodeAPIActions postCodeAPIActions;

    @Steps
    CommonQuestions commonQuestions;

    @Given("the user queries postcode endpoint with (.+)$")
    public void theUserQueriesEndpointWithPostcode(String postcode) {
        postCodeAPIActions.callPostCodeAPI(postcode);
    }

    @Then("the response status code should be (.+)$")
    public void theStatusCodeIsStatuscode(Integer statuscode) {
        commonQuestions.verifyStatuscode(statuscode);
    }

    @And("the schema should match with the specification defined in (.*)$")
    public void theSchemeShouldMatchWithSpecDefinedIn(String spec) {
        commonQuestions.verifyResponseSchema(spec);
    }

    @Then("verify the below details in the response")
    public void verifyTheBelowDetailsInTheResponse(DataTable dataTable) {
        List<List<String>> rows = dataTable.asLists(String.class);
        for (List<String> columns : rows) {
            commonQuestions.verifyResponseKeyValue(columns.get(0), columns.get(1));
        }
    }
}
