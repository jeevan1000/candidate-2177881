package com.homeoffice.assignment.apiutils;

import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class PostCodeAPIActions {

    /*
     * This class contain the steps that can be called on PostCode API endpoint.
     * BASE_URL will be fetched from the serenity.conf file  with getProperty method
     */

    private EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();
    private String BASE_URL = EnvironmentSpecificConfiguration.from(env).getProperty("postcodes.api.endpoint");

    @Step("Call PostCode endpoint with the postcode : {0}")
    public void callPostCodeAPI(String postcode) {
        String url = BASE_URL;
        Response response = SerenityRest
                .given()
                .pathParam("postcode", postcode)
                .header("Accept", "application/json")
                .when()
                .get(url + "{postcode}");
    }
}
